<?php

/**
 * @file
 * Admin page callbacks for the content unpublish module.
 */

function content_unpublish_admin_settings() {
  $form = array();
  $form['content_unpublish_button_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Button label'),
    '#default_value' => variable_get('content_unpublish_button_name', t('Unpublish')),
    '#description' => t("The label that will appear on the 'Unpublish' button."),
  );
  return system_settings_form($form);
}

